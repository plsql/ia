import java.util.Arrays;

public class StringToMatrice {

	public static int[][] strToMtrx(String in) {
		String[] out1 = in.split("\\|");
		String[] out2 = out1[0].split(":");
		int[][] out = new int[out1.length][out2.length];

		for (int i = 0; i < out1.length; ++i) {
			String[] tmp = out1[i].split(":");
			int[] line = new int[tmp.length];
			int t = 0;
			for (String str : tmp) {
				line[t] = Integer.parseInt(str);
				++t;
			}
			out[i] = line;
		}

		return out;
	}
}
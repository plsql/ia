import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class RandomMove {

	private final static int NB_MAX_PARECELLE = 64;

	private static ArrayList<Character> tabLettres = new ArrayList<Character>(Arrays.asList('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'));
	private static ArrayList<String> myMoves = new ArrayList<>();
	private static ArrayList<String> hisMoves = new ArrayList<String>();
	private static ArrayList<String> ownMoves = new ArrayList<String>();
	private static char[][] mapArray;
	private static int col = 0, row = 0;

	public static String randomFirstMove() {
		Random r = new Random();
		int n = r.nextInt(NB_MAX_PARECELLE - 1);

		int i = 0;

		int colonne = 1;
		int ligne = 1;

		for (char[] subtab : mapArray) {
			for (char sub : subtab) {
				if (Character.isLowerCase(sub)) {
					if (i == n) {
						myMoves.add(tabLettres.get(colonne - 1) + ":" + ligne);
						return tabLettres.get(colonne - 1) + ":" + ligne;
					} else {
						i++;
					}
				}
				colonne = colonne + 1;
			}
			colonne = 1;
			ligne = ligne + 1;
		}
		return null;
	}
	
	public static String getNextMove() {
		if(col == 10) col = 0;
		if(row == 10) row = 0;
		return tabLettres.get(col++) + ":" + (row++ + 1);
	}
	
	public static void setMap(char[][] mat) {
		mapArray = mat;
	}
	
	public static String enhancedRandomMove(String enemyMove) {
		if(!hisMoves.contains(enemyMove))
			hisMoves.add(enemyMove);
		String newMove = null;
		do {
			newMove = getNextMove();
		} while(!isMoveValid(newMove));
		ownMoves.add(newMove);
		col = 0;
		row = 0;
		return newMove;
	}

	private static boolean isMoveValid(String newMove) {
		if(newMove == null)
			return false;
		if(hisMoves.contains(newMove) || ownMoves.contains(newMove))
			return false;
		String[] splittedEnemy = hisMoves.get(hisMoves.size()-1).split(":");
		int enX = tabLettres.indexOf(splittedEnemy[0].charAt(0));
		int enY = Integer.parseInt(splittedEnemy[1]);
		String[] splitted = newMove.split(":");
		int xValue = tabLettres.indexOf(splitted[0].charAt(0));
		int yValue = Integer.parseInt(splitted[1]);
		if(enX != xValue && enY != yValue)
			return false;
		if(mapArray[xValue][yValue-1] == mapArray[enX][enY-1]) {
			return false;
		}
		if(mapArray[xValue][yValue-1] == 'M' || mapArray[xValue][yValue-1] == 'F')
			return false;
		if(ownMoves.size() > 0) {
			String[] splittedLastMe = ownMoves.get(ownMoves.size()-1).split(":");
			int lastMeX = tabLettres.indexOf(splittedLastMe[0].charAt(0));
			int lastMeY = Integer.parseInt(splitted[1]);
			if(mapArray[xValue][yValue-1] == mapArray[lastMeX][lastMeY-1])
				return false;
		}
		return true;
	}

	/*
	public static String responseMove(String previousMove, char[][] matrice) {
		if (previousMove == null) {
			return randomFirstMove(matrice);
		}

		hisMoves.add(previousMove);

		return randomMove(previousMove, matrice);
	}

	public static String randomMove(String previousMove, char[][] matrice) {

		// Previous Move datas
		char colonnePreviousMove = previousMove.charAt(0);
		int indexColonnePreviousMove = 0;
		for (char elem : tabLettres) {
			if (colonnePreviousMove != elem) {
				++indexColonnePreviousMove;
			} else {
				break;
			}
		}
		int lignePreviousMove = Integer.parseInt(previousMove.split(":")[1]) - 1;

		// Pre-Previous Move datas
		if (!myMoves.isEmpty()) {
			String ppMove = myMoves.get(myMoves.size() - 1);

			char colonnePPMove = ppMove.charAt(0);
			int indexColonnePPMove = 0;
			for (char elem : tabLettres) {
				if (colonnePPMove != elem) {
					++indexColonnePPMove;
				} else {
					break;
				}
			}
			int lignePPMove = Integer.parseInt(ppMove.split(":")[1]) - 1;

			for (int i = 0; i < 10; i++) {
				// par ligne
				// if lower case and not previous letter and not previous previous letter
				if (Character.isLowerCase(matrice[i][indexColonnePreviousMove])
						&& matrice[lignePreviousMove][indexColonnePreviousMove] != matrice[i][indexColonnePreviousMove]
						&& matrice[lignePPMove][indexColonnePPMove] != matrice[i][indexColonnePPMove]) {
					myMoves.add(tabLettres[indexColonnePreviousMove] + ":" + (lignePreviousMove + 1));
					return tabLettres[indexColonnePreviousMove] + ":" + (lignePreviousMove + 1);
				}

				// par colonne
				if (Character.isLowerCase(matrice[lignePreviousMove][i])
						&& matrice[lignePreviousMove][indexColonnePreviousMove] != matrice[lignePreviousMove][i]
						&& matrice[lignePPMove][indexColonnePPMove] != matrice[lignePPMove][i]) {
					myMoves.add(tabLettres[indexColonnePreviousMove] + ":" + (lignePreviousMove + 1));
					return tabLettres[indexColonnePreviousMove] + ":" + (lignePreviousMove + 1);
				}
			}
		} else {
			for (int i = 0; i < 10; i++) {
				// par ligne
				// if lower case and not previous letter and not previous previous letter
				if (Character.isLowerCase(matrice[i][indexColonnePreviousMove])
						&& matrice[lignePreviousMove][indexColonnePreviousMove] != matrice[i][indexColonnePreviousMove]) {
					myMoves.add(tabLettres[indexColonnePreviousMove] + ":" + (lignePreviousMove + 1));
					return tabLettres[indexColonnePreviousMove] + ":" + (lignePreviousMove + 1);
				}

				// par colonne
				if (Character.isLowerCase(matrice[lignePreviousMove][i])
						&& matrice[lignePreviousMove][indexColonnePreviousMove] != matrice[lignePreviousMove][i]) {
					myMoves.add(tabLettres[indexColonnePreviousMove] + ":" + (lignePreviousMove + 1));
					return tabLettres[indexColonnePreviousMove] + ":" + (lignePreviousMove + 1);
				}
			}
		}
		return null;
	}*/

}

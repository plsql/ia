package utils;

public class Convert {
	public static String intToBin4(int value) {
		int valueMod = value;
		String str = new String();
		for(int i = 8; i >= 1; i = i/2) {
			if(valueMod >= i) {
				str += "1";
				valueMod -= i;
			} else str += "0";
		}
		return str;
	}
}

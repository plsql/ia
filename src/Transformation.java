import utils.Convert;

public class Transformation {
	
	public static char[][] transmutation(int[][] tab){
		char[] ArrayChar = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j','k', 'l', 'm','n','o','p','q'};
		int[][] tabCp = tab;
		int parcelle = 0;
		char[][] tabRet = new char[tab.length][tab.length];
			for (int x = 0; x<tab.length; x++) {
				for(int y = 0; y<tab.length; y++) {
					// VERIFICATION MER & FORET
					if (tab[x][y]>=32) 
						if(tab[x][y]>=64)
							tabRet[x][y] = 'M';
						else
							tabRet[x][y] = 'F';
					if(tab[x][y]==tab[x][0])
						tabCp[x][y] -= 2;
					else if(tab[x][y]==tab[0][y])
						tabCp[x][y] -=1;
					else if(tab[x][y]==tab[x][tab.length-1])
						tabCp[x][y] -=4;
					else if(tab[x][y]==tab[tab.length-1][y])
						tabCp[x][y] -=3;
				}
			}
			for (int y = 0; y<tab.length-1; y++) {
				if (tabRet[y][0]!='M' || tabRet[y][0]!='F') {
					tabRet[y][0]=ArrayChar[parcelle];
					if (Convert.intToBin4(tabRet[y+1][0]).charAt(2)==1 && Convert.intToBin4(tabRet[y+1][0]).charAt(3)==1) {
						parcelle++;
					}
				}
			}for (int y = 0; y<tab.length-1; y++) {
				if (tabRet[0][y]!='M' || tabRet[0][y]!='F') {
					tabRet[0][y]=ArrayChar[parcelle];
					if (Convert.intToBin4(tabRet[0][y+1]).charAt(2)==1 && Convert.intToBin4(tabRet[0][y+1]).charAt(3)==1) {
						parcelle++;
					}
				}
			}
			for (int x = 1; x<tab.length; x++) {
				tabRet[x][0] = ArrayChar[ ElementGauche(x, 1, tabCp, ArrayChar, tabRet)];
				for(int y = 1; y<tab.length; y++) {
					if (tabRet[y][x]!='M' || tabRet[y][x]!='F') {
						if (Convert.intToBin4(tabRet[x][y-1]).charAt(3)==0) 
							tabRet[x][y]=tabRet[x][y-1];
						else if (Convert.intToBin4(tabRet[x-1][y]).charAt(2)==0)
							tabRet[x][y]=tabRet[x-1][y];
					}
				}
			}
			return tabRet;
	}
	//x,y pos dans le tableau d'int, tab tableau d'int, CharTab 
	public static int ElementGauche(int x, int y, int[][] tab, char[] CharTab, char[][] RetTab) {
		if (Convert.intToBin4(tab[x][y-1]).charAt(2)==1)
			return FindChar(RetTab[x][y], CharTab);
		else
			return FindChar(RetTab[tab.length-1][y], CharTab)+1;
		
		
	}
	public static int FindChar(char x, char[] tab) {
		for (int i = 0; i < tab.length; i++)
			if (x==tab[i])
				return i;
		return 0;
	}
}

import java.util.Arrays;

import utils.Convert;

public class Transformation2 {
	private static char[] ArrayChar = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j','k', 'l', 'm','n','o','p','q'};
	private static int currentIndex = 0;
	private static char[][] tabOut;
	
	public static char[][] transmutation(int[][] tabIn) {
		tabOut = new char[tabIn.length][tabIn.length];
		for(int i = 0; i < tabIn.length; i++) {
			for(int j = 0; j < tabIn.length; j++) {
				tabOut[i][j] = ' ';
				// Mers
				if(tabIn[i][j] >= 64)
					tabOut[i][j] = 'M';
				else if(tabIn[i][j] >= 32)
					tabOut[i][j] = 'F';
				else {
					String binValue = Convert.intToBin4(tabIn[i][j]);
					char value = getMove(i, j, binValue);
					tabOut[i][j] = value;
				}
			}
		}
		return tabOut;
	}
	
	private static char getMove(int i, int j, String binValue) {
		if(binValue.charAt(0) == '0' && tabOut[i][j+1] != ' ' && tabOut[i][j+1] != (char)0) {
			return tabOut[i][j+1];
		} else if(binValue.charAt(1) == '0' && tabOut[i+1][j] != ' ' && tabOut[i+1][j] != (char)0) {
			return tabOut[i+1][j];
		} else if(binValue.charAt(2) == '0' && tabOut[i][j-1] != ' ' && tabOut[i][j-1] != (char)0) {
			return tabOut[i][j-1];
		} else if(binValue.charAt(3) == '0' && tabOut[i-1][j] != ' ' && tabOut[i-1][j] != (char)0) {
			return tabOut[i-1][j];
		}
		return ArrayChar[currentIndex++];
	}
}

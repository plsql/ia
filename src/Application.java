import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Arrays;

public class Application {
	private static final String HOTE = "172.16.97.194";
	private static final int PORT = 8015;

	public static void main(String[] args) {
		System.out.println("Connexion au serveur...");
		try {

			DatagramSocket clientSock = new DatagramSocket();
			InetAddress IPAddress = InetAddress.getByName(HOTE);
			clientSock.connect(IPAddress, PORT);
			byte[] sendData = "EquipeParis1".getBytes();
			byte[] receiveData = new byte[1024];
			DatagramPacket out = new DatagramPacket(sendData, sendData.length, IPAddress, PORT);
			DatagramPacket in = new DatagramPacket(receiveData, receiveData.length);
			// first connection
			clientSock.send(out);
			clientSock.receive(in);
			System.out.println(new String(in.getData()));

			// get table data
			clientSock.receive(in);
			// String mapData =
			// "3:9:71:69:65:65:65:65:65:73|2:8:3:9:70:68:64:64:64:72|6:12:2:8:3:9:70:68:64:72|11:11:6:12:6:12:3:9:70:76|10:10:11:11:67:73:6:12:3:9|14:14:10:10:70:76:7:13:6:12|3:9:14:14:11:7:13:3:9:75|2:8:7:13:14:3:9:6:12:78|6:12:3:1:9:6:12:35:33:41|71:77:6:4:12:39:37:36:36:44";
			String mapData = new String(in.getData());
			System.out.println(mapData);
			mapData = mapData.split("=")[1];
			mapData = mapData.trim();
			mapData = mapData.substring(0, mapData.length() - 1);
			System.out.println(mapData);

			// appel fonction
			int[][] mapIntArray = StringToMatrice.strToMtrx(mapData);
			System.out.println(Arrays.deepToString(mapIntArray));
			char[][] mapCharArray = Transformation2.transmutation(mapIntArray);
			System.out.println(Arrays.deepToString(mapCharArray));
			RandomMove.setMap(mapCharArray);

			for (int k = 0; k < 27; k++) {
				String moveEnemy = null;
				boolean loop = true;
				while (loop) {
					in.setData(new byte[1024]);
					clientSock.receive(in);
					String completeResp = new String(in.getData()).trim();
					String response = completeResp.substring(0, 2).trim();
					if (response.equals("10"))
						loop = false;
					else if (response.equals("20")) {
						String[] moveEnemyTab = completeResp.split(":");
						moveEnemy = moveEnemyTab[moveEnemyTab.length - 2] + ":" + moveEnemyTab[moveEnemyTab.length - 1];
					}else if (response.equals("80")) {
						System.out.println("on a win !!");
						return;
					}
				}
				System.out.println("ENEMY : " + moveEnemy);
				String move = RandomMove.enhancedRandomMove(moveEnemy);
				if (move == null)
					move = "B:7";
				System.out.println("ME: " + move);
				out.setData(move.getBytes());
				clientSock.send(out);
				System.out.println(new String(out.getData()));
				System.out.println("Tour " + k + " termin�");
			}
		} catch (Exception e) {
			System.out.println("Erreur lors de la connexion au serveur");
			e.printStackTrace();
		}
	}
}
